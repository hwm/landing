---
title: Hola mundo :)
description: Como llegué hasta acá.
layout: post
image: /assets/hola-mundo.webp
date: 2020-01-01 00:00:00 -0300
---

Después de casi 2 años desarrollando diferentes versiones y aprendiendo tecnologías nuevas y viejas, quiero creer haber terminado la última primera versión del Manejador de Tareas.

Ni siquiera estoy seguro como se me ocurrió la idea. Estaba pensando en que proyecto empezar, y de alguna manera llegué a la idea del Manejador, sin saber por qué, o que tenía de bueno o mejor a otras opciones, o tantas otras cosas que no pensé por ninguna razón justificable. Pero se me ocurrió, y lo empecé a hacer.

Después de más de dos años, quiero intentar cumplir con algo: **nunca más volver a reescribir** el Manejador.

Pueden probar esta versión en [app.tareas.nulo.in](//app.tareas.nulo.in). Como en todas las versiones, completamente gratuita y [código libre](//0xacab.org/hwm). Espero que les sirva.
