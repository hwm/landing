let plugins = [
  require("tailwindcss"),
]

if (process.env.JEKYLL_ENV === 'production') {
	plugins.push(require('cssnano'))
	plugins.push(require('autoprefixer'))
}

module.exports = {
	plugins,
};
