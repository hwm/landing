all: push build deploy

push:
	git push

build:
	JEKYLL_ENV=production bundle exec jekyll build
	yarn tailwind build css/site.css > _site/css/site.css

deploy:
	rsync -rtuP _site/ pi@nulo.in:/var/www/tareas.nulo.in/
