# frozen_string_literal: true

module Jekyll
  # filter to format date in spanish
  module FormatDateSpanish
    MONTHS = %w[Enero Febrero Marzo Abril Mayo Junio Julio Agosto Septiembre Octubre Noviembre Diciembre].freeze
    def format_date_spanish(date)
      "#{date.day} de #{MONTHS[date.month - 1]}, #{date.year}"
    end
  end
end

Liquid::Template.register_filter(Jekyll::FormatDateSpanish)
